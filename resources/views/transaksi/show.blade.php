@extends('layouts.master')
@section('content')
<div class="ml-3 mt-3">
    <h2> Details Transaksi </h2>
    <p> {{ $transaksi->tgl_sewa }} </p>
    <p> Rp.{{ $transaksi->harga }} </p>
    <p> {{ $transaksi->jenis_sewa }} </p>
    <p> {{ $transaksi->penyewa->name }} </p>
    <p> {{ $transaksi->kontrakan->name }} </p>
</div>
@endsection