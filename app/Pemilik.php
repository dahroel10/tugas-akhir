<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Pemilik extends Model
{
    public function kontrakans()
    {
        return $this->hasMany('App\Kontrakan');
    }

    protected $table = "pemilik";
    protected $fillable = ["name", "alamat", "email", "norek", "user_id"];

}
