@extends('layouts.master')
@section('content')
<div class="ml-3 mt-3">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Daftar Pemilik Kontrakan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      @if (session('success'))
          <div class="alert alert-success">
            {{( session ('success'))}}
          </div>
      @endif
      <a class="btn btn-info mb-4" href="{{ route('pemilik.create') }}">
        Daftar Sebagai Pemilik Kontrakan Baru
      </a>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th style="width: 200px">Nama Pemilik</th>
            <th>Alamat</th>
            <th>Email</th>
            <th>No Rekening</th>
            <th style="width: 120px">Actions</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($pemiliks as $key => $pemilik)
            <tr>
              <td> {{ $key + 1 }} </td>
              <td> {{ $pemilik->name }} </td>
              <td> {{ $pemilik->alamat }} </td>
              <td> {{ $pemilik->email }} </td>
              <td> {{ $pemilik->norek }} </td>
              @auth
              <td style="display : flex;"> 
                <a href="{{route('pemilik.show', ['pemilik' => $pemilik->id])}}" class="btn btn-info btn-sm"> Lihat </a>
                <a href="{{route('pemilik.edit', ['pemilik' => $pemilik->id])}}" class="btn btn-default btn-sm"> Edit </a>
                <form action="{{route('pemilik.destroy', ['pemilik' => $pemilik->id ])}}" method="post">
                  @csrf
                  @method('DELETE')
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
              </td>
            </tr>
              @endauth
              @guest
              <td style="display : flex;"> 
              <a href="{{route('pemilik.show', ['pemilik' => $pemilik->id])}}" class="btn btn-info btn-sm"> Lihat </a>
              </td>
              @endguest
            @empty
              <tr>
                <td colspan="6" align="center"> Tidak Ada Daftar Pemilik </td>
              </tr>
          @endforelse
        </tbody>
      </table>
      
    </div>
    <!-- /.card-body -->
  </div>
</div>


@endsection