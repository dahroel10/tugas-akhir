@extends('layouts.master')
@section('content')
<div class="ml-3 mt-3">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Daftar Kontrakan</h3>
      <br>
      @auth
      <a href="/kontrakan/create" class="btn btn-success my-2">Tambah Kontrakan Baru</a>          
      @endauth

    <!-- /.card-header -->
    <div class="row">
      @foreach ($kontrakans as $item)
      <div class="card mx-1">
        <img src="{{ asset('img/kontrakan/'. $item->foto) }}" width="250px" alt="...">
        <div class="card-body">
          <h5 class="card-title">{{ $item->name }}</h5>
          <p class="card-text">Alamat : {{ $item->alamat }}</p>
          @auth
          <a href="/kontrakan/{{ $item->id }}" class="btn btn-primary">Lihat Detail</a>
          <a href="/kontrakan/{{ $item->id }}/edit" class="btn btn-info">Edit</a>
          <form action="/kontrakan/{{ $item->id }}" method="POST">
            @method('delete')
            @csrf
            <input type="Submit" class="btn btn-danger" value="Delete">
          </form>
          @endauth
          @guest
          <a href="/kontrakan/{{ $item->id }}" class="btn btn-primary">Lihat Detail</a>
          @endguest
          <p class="card-text"><small class="text-muted">{{ $item->created_at }}</small></p>
        </div>
      </div>
      
      @endforeach
    </div>
  </div>
</div>
</div>


@endsection