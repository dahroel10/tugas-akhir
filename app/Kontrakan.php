<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kontrakan extends Model
{   
    public function pemilik(){
        return $this->belongsTo('App\Pemilik', "pemilik_id");
    }

    protected $table = "kontrakan";
    protected $fillable = ["name", "alamat", "fasilitas", "harga", "jumlah_kontrakan", "foto", "pemilik_id"];
}
