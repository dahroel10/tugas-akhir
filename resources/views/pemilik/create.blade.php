@extends('layouts.master')
@section('content')

<div class="ml-3 mt-3">
    <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Daftar Sebagai Pemilik Baru</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/pemilik" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Nama Pemilik</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ Auth::user()->name }}" placeholder="Masukkan Nama Anda">
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="text" class="form-control" id="alamat" name="alamat" value="{{ old('alamat', '') }}" placeholder="Masukkan Alamat Anda">
                        @error('alamat')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" name="email" value="{{ old('email', '') }}" placeholder="Masukkan Alamat Email Anda">
                        @error('alamat')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="norek">No Rekening</label>
                        <input type="text" class="form-control" id="norek" name="norek" value="{{ old('norek', '') }}" placeholder="Masukkan No Rekening Anda">
                        @error('norek')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
     </div>
</div>

@endsection