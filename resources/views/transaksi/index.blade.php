@extends('layouts.master')
@section('content')
<div class="ml-3 mt-3">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Daftar Transaksi</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      @if (session('success'))
          <div class="alert alert-success">
            {{( session ('success'))}}
          </div>
      @endif
      <a class="btn btn-info mb-4" href="{{ route('transaksi.create') }}">
        Sewa Kontrakan Baru
      </a>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th>Tanggal Sewa</th>
            <th>Harga</th>
            <th>Jenis Sewa</th>
            <th>Penyewa</th>
            <th>Kontrakan</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($transaksi as $key => $transaksi)
            <tr>
              <td> {{ $key + 1 }} </td>
              <td> {{ $transaksi->tgl_sewa }} </td>
              <td> {{ $transaksi->harga }} </td>
              <td> {{ $transaksi->jenis_sewa }} </td>
              <td> {{ $transaksi->penyewa->name }} </td>
              <td> {{ $transaksi->kontrakan->name }} </td>
              @auth
              <td style="display : flex;"> 
                <a href="{{route('transaksi.show', ['transaksi' => $transaksi->id])}}" class="btn btn-info btn-sm"> Lihat </a>
                <a href="{{route('transaksi.edit', ['transaksi' => $transaksi->id])}}" class="btn btn-default btn-sm"> Edit </a>
                <form action="{{route('transaksi.destroy', ['transaksi' => $transaksi->id ])}}" method="post">
                  @csrf
                  @method('DELETE')
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
              </td>
            </tr>
              @endauth
            @empty
              <tr>
                <td colspan="7" align="center"> Tidak Ada Daftar Transaksi </td>
              </tr>
          @endforelse
        </tbody>
      </table>
      
    </div>
    <!-- /.card-body -->
  </div>
</div>
@endsection