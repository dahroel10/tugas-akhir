@extends('layouts.master')
@section('content')
<div class="ml-3 mt-3">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Daftar Penyewa Kontrakan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      @if (session('success'))
          <div class="alert alert-success">
            {{( session ('success'))}}
          </div>
      @endif
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th style="width: 200px">Nama Penyewa</th>
            <th>Jenis Kelamin</th>
            <th>Alamat</th>
            <th>Email</th>
            <th>No HP</th>
            <th>No KTP</th>
            <th>Pekerjaan</th>
            <th>Foto</th>
            <th style="width: 120px">Actions</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($penyewas as $key => $penyewa)
            <tr>
              <td> {{ $key + 1 }} </td>
              <td> {{ $penyewa->name }} </td>
              <td> {{ $penyewa->jk }} </td>
              <td> {{ $penyewa->alamat }} </td>
              <td> {{ $penyewa->email }} </td>
              <td> {{ $penyewa->no_hp }} </td>
              <td> {{ $penyewa->no_ktp }} </td>
              <td> {{ $penyewa->pekerjaan }} </td>
              <td> <img src="{{ asset('img/penyewa/'. $penyewa->foto) }}" width="100px" alt="..."> </td>
              @auth
              <td style="display : flex;"> 
                <a href="{{route('penyewa.show', ['penyewa' => $penyewa->id])}}" class="btn btn-info btn-sm"> Lihat </a>
                <a href="{{route('penyewa.edit', ['penyewa' => $penyewa->id])}}" class="btn btn-default btn-sm"> Edit </a>
                <form action="{{route('penyewa.destroy', ['penyewa' => $penyewa->id ])}}" method="post">
                  @csrf
                  @method('DELETE')
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
              </td>
            </tr>
              @endauth
              @guest
              <td style="display : flex;"> 
              <a href="{{route('penyewa.show', ['penyewa' => $penyewa->id])}}" class="btn btn-info btn-sm"> Lihat </a>
              </td>
              @endguest
            @empty
              <tr>
                <td colspan="6" align="center"> Tidak Ada Daftar Penyewa </td>
              </tr>
          @endforelse
        </tbody>
      </table>
      <a class="btn btn-info mt-4" href="{{ route('penyewa.create') }}">
        Daftar Sebagai Penyewa Kontrakan Baru
      </a>
    </div>
    <!-- /.card-body -->
  </div>
</div>


@endsection