<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Kontrakan;
use App\Pemilik;
use File;

class KontrakanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
        $this->middleware('checkRole:pemilik')->except('index', 'show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kontrakans = Kontrakan::all();
        return view('kontrakans.index', compact('kontrakans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pemilik = Pemilik::all();
        return view('kontrakans.create', compact('pemilik'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'alamat'=>'required',
            'fasilitas'=>'required',
            'harga'=>'required',
            'jumlah_kontrakan'=>'required',
            'foto'=>'required|mimes:jpeg,jpg,png|max:2200',
            'pemilik_id' => 'required'
        ]);

        $gambar = $request->foto;
        $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();

        Kontrakan::create([
            'name'=>$request->name,
            'alamat'=>$request->alamat,
            'fasilitas'=>$request->fasilitas,
            'harga'=>$request->harga,
            'jumlah_kontrakan'=>$request->jumlah_kontrakan,
            'foto'=>$new_gambar,
            'pemilik_id'=>$request->pemilik_id
        ]);
        $gambar -> move('img/kontrakan/', $new_gambar);
        return redirect('/kontrakan')->with('success', 'Berhasil Menambahkan Kontrakan Baru!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kontrakan = Kontrakan::find($id);
        return view('kontrakans.show', compact('kontrakan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $kontrakan = Kontrakan::find($id);
        $pemilik = Pemilik::all();
        return view('kontrakans.edit', compact('kontrakan', 'pemilik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
            'alamat'=>'required',
            'fasilitas'=>'required',
            'harga'=>'required',
            'jumlah_kontrakan'=>'required',
            'foto'=>'mimes:jpeg,jpg,png|max:2200',
            'pemilik_id' => 'required'
        ]);
        
        $kontrakan = Kontrakan::findorfail($id);
        
        if($request->has('foto')){
            $path = 'img/kontrakan/';
            File::delete($path . $kontrakan->foto);
            $gambar = $request->foto;
            $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();
            $gambar->move($path, $new_gambar);
            $kontrakan_data = [
                'name'=>$request->name,
                'alamat'=>$request->alamat,
                'fasilitas'=>$request->fasilitas,
                'harga'=>$request->harga,
                'jumlah_kontrakan'=>$request->jumlah_kontrakan,
                'foto'=>$new_gambar,
                'pemilik_id'=>$request->pemilik_id
            ];
        } else{
            $kontrakan_data = [
                'name'=>$request->name,
                'alamat'=>$request->alamat,
                'fasilitas'=>$request->fasilitas,
                'harga'=>$request->harga,
                'jumlah_kontrakan'=>$request->jumlah_kontrakan,
                'pemilik_id'=>$request->pemilik_id
            ];
        }
        $kontrakan->update($kontrakan_data);
        return redirect('/kontrakan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kontrakan = Kontrakan::findorfail($id);
        $kontrakan -> delete();

        $path = "img/kontrakan/";
        File::delete($path . $kontrakan->foto);
        
        return redirect('/kontrakan');
    }
}
