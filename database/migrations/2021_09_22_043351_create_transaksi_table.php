<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tgl_sewa');
            $table->integer('harga');
            $table->string('jenis_sewa');
            $table->unsignedBigInteger('penyewa_id');
            $table->foreign('penyewa_id')->references('id')->on('penyewa');  
            $table->unsignedBigInteger('kontrakan_id');
            $table->foreign('kontrakan_id')->references('id')->on('kontrakan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
        $table->dropForeign(['penyewa_id']);
        $table->dropColumn(['penyewa_id']);
        $table->dropForeign(['kontrakan_id']);
        $table->dropColumn(['kontrakan_id']);
    }
}
