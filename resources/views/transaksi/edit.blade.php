@extends('layouts.master')
@section('content')

    <div class="ml-3 mt-3">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Edit Data Transaksi {{ $transaksi->id }}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="/transaksi/{{ $transaksi->id }}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="tgl_sewa">Tanggal Sewa</label>
                        <input type="date" class="form-control" id="tgl_sewa" name="tgl_sewa"
                            value="{{ old('tgl_sewa', $transaksi->tgl_sewa) }}"
                            placeholder="Masukkan Nama Kontrakan Anda">
                        @error('tgl_sewa')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="harga">Harga</label>
                        <input type="number" class="form-control" id="harga" name="harga"
                            value="{{ old('harga', $transaksi->harga) }}">
                        @error('harga')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="jenis_sewa">Jenis Sewa</label>
                        <input type="text" class="form-control" id="jenis_sewa" name="jenis_sewa"
                            value="{{ old('jenis_sewa', $transaksi->jenis_sewa) }}"
                            placeholder="Masukkan Jenis Sewa Kontrakan Anda">
                        @error('jenis_sewa')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="penyewa">Penyewa</label>
                        <select class="form-control" id="penyewa" name="penyewa_id">
                            @foreach ($penyewas as $penyewa)
                                <option value="{{ $penyewa->id }}"
                                    {{ $transaksi->penyewa_id == $penyewa->id ? 'selected' : '' }}>{{ $penyewa->name }}
                                </option>
                            @endforeach
                        </select>
                        @error('penyewa')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="kontrakan">Kontrakan</label>
                        <select class="form-control" id="kontrakan" name="kontrakan_id">
                            @foreach ($kontrakans as $kontrakan)
                                <option value="{{ $kontrakan->id }}"
                                    {{ $transaksi->kontrakan_id == $kontrakan->id ? 'selected' : '' }}>
                                    {{ $kontrakan->name }}
                                </option>
                            @endforeach
                        </select>
                        @error('kontrakan')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>

@endsection
