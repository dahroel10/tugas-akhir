@extends('layouts.master')
@section('content')

<div class="ml-3 mt-3">
    <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Daftar Kontrakan Baru</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/kontrakan" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Nama Kontrakan</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', '') }}" placeholder="Masukkan Nama Kontrakan Anda">
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="text" class="form-control" id="alamat" name="alamat" value="{{ old('alamat', '') }}" placeholder="Masukkan Alamat Kontrakan Anda">
                        @error('alamat')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="fasilitas">Fasilitas</label>
                        <input type="text" class="form-control" id="fasilitas" name="fasilitas" value="{{ old('fasilitas', '') }}" placeholder="Masukkan Fasilitas Kontrakan Anda">
                        @error('alamat')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="harga">Harga</label>
                        <input type="text" class="form-control" id="harga" name="harga" value="{{ old('harga', '') }}" placeholder="Masukkan Harga Kontrakan Anda">
                        @error('harga')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="jumlah_kontrakan">Jumlah Kontrakan</label>
                        <input type="text" class="form-control" id="jumlah_kontrakan" name="jumlah_kontrakan" value="{{ old('jumlah_kontrakan', '') }}" placeholder="Masukkan Jumlah Kontrakan Kontrakan Anda">
                        @error('jumlah_kontrakan')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="foto">Foto</label>
                        <input type="file" class="form-control" id="foto" name="foto">
                        @error('foto')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="pemilik_id">Nama Pemilik</label>
                        <select class="form-control" name="pemilik_id" id="pemilik_id">
                            <option value="">-- Pilih Nama Pemilik --</option>
                            @foreach ($pemilik as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @error('pemilik_id')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
     </div>
</div>

@endsection