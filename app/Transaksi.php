<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    public function Penyewa(){
        return $this->belongsTo('App\Penyewa');
    }

    public function Kontrakan(){
        return $this->belongsTo('App\Kontrakan');
    }

    protected $table = "transaksi";
    protected $fillable = ["tgl_sewa", "harga", "jenis_sewa", "penyewa_id", "kontrakan_id"];
}
