@extends('layouts.master')
@section('content')
<div class="ml-3 mt-3">
    <h2> Details Pemilik </h2>
    <p> {{ $pemilik->name }} </p>
    <p> {{ $pemilik->alamat }} </p>
    <p> {{ $pemilik->email }} </p>
    <p> {{ $pemilik->norek }} </p>
</div>
@endsection