<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Kontrakan;
use App\Pemilik;
use Auth;

class PemilikController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
        $this->middleware('checkRole:pemilik');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pemiliks = Pemilik::all();
        return view('pemilik.index', compact('pemiliks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pemilik.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'alamat'=>'required',
            'email'=>'required',
            'norek'=>'required'
        ]);

        $pemilik = Pemilik::create([
            "name"=> $request["name"],
            "alamat"=>$request["alamat"],
            "email"=>$request["email"],
            "norek"=>$request["norek"]
        ]);
        return redirect('/pemilik')->with('success', 'Berhasil Menambahkan Pemilik Baru!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pemilik = Pemilik::find($id);
        return view('pemilik.show', compact('pemilik'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pemilik = Pemilik::find($id);
        return view('pemilik.edit', compact('pemilik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'alamat'=>'required',
            'email'=>'required',
            'norek'=>'required'
        ]);

        $update = Pemilik::where('id', $id)->update([
            "name"=>$request["name"],
            "alamat"=>$request["alamat"],
            "email"=>$request["email"],
            "norek"=>$request["norek"]
        ]);
        return redirect('/pemilik')->with('success', 'Berhasil Merubah Data Pemilik!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pemilik::destroy($id);
        return redirect('/pemilik')->with('success', 'Data Pemilik  Berhasil Dihapus');
    }
}
