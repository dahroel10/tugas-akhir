<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penyewa extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    protected $table = "penyewa";
    protected $fillable = ["name", "jk", "alamat", "email", "no_hp", "no_ktp", "pekerjaan", "foto", "user_id"];

}
