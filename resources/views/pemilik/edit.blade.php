@extends('layouts.master')
@section('content')

<div class="ml-3 mt-3">
    <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Data Pemilik {{ $pemilik->name }}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/kontrakan/{{ $pemilik->id }}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Nama Pemilik</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $pemilik->name) }}" placeholder="Masukkan Nama Anda">
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="text" class="form-control" id="alamat" name="alamat" value="{{ old('alamat', $pemilik->alamat) }}" placeholder="Masukkan Alamat Anda">
                        @error('alamat')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" name="email" value="{{ old('email', $pemilik->email) }}" placeholder="Masukkan Email Anda">
                        @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="norek">Norek</label>
                        <input type="text" class="form-control" id="norek" name="norek" value="{{ old('norek', $pemilik->norek) }}" placeholder="Masukkan Norek Anda">
                        @error('norek')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
     </div>
</div>

@endsection