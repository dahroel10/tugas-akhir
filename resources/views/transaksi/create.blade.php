@extends('layouts.master')
@section('content')

<div class="ml-3 mt-3">
    <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Membuat Transaksi Baru</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/transaksi" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="tgl_sewa">Tanggal Sewa</label>
                        <input type="date" class="form-control" id="tgl_sewa" name="tgl_sewa" value="{{ old('tgl_sewa', '') }}>
                        @error('tgl_sewa')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="harga">Harga</label>
                        <input type="number" class="form-control" id="harga" name="harga" value="{{ old('harga', '') }}">
                        @error('harga')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="jenis_sewa">Jenis Sewa</label>
                        <select class="form-control" name="jenis_sewa" id="jk">
                            <option value="">-- Pilih Jenis Sewa Kontrakan Anda --</option>
                            <option value="1_bulan">1 Bulan</option>
                        </select>
                        @error('jk')
                            <span class="invalid-feedback" jk="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="penyewa_id">Penyewa</label>
                        <select class="form-control" id="platform" name="penyewa_id">
                            @foreach ($penyewas as $penyewa)
                                <option value="{{ $penyewa->id }}">{{ $penyewa->name }}</option>
                            @endforeach
                        </select>
                        @error('penyewa_id')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="kontrakan_id">Kontrakan</label>
                        <select class="form-control" id="platform" name="kontrakan_id">
                            @foreach ($kontrakans as $kontrakan)
                                <option value="{{ $kontrakan->id }}">{{ $kontrakan->name }}</option>
                            @endforeach
                        </select>
                        @error('kontrakan')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
     </div>
</div>

@endsection