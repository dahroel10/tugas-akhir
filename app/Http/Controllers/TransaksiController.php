<?php

namespace App\Http\Controllers;

use App\Kontrakan;
use App\Penyewa;
use App\Transaksi;
use Illuminate\Http\Request;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksi = Transaksi::all();
        return view('transaksi.index', compact('transaksi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kontrakans = Kontrakan::all();
        $penyewas = Penyewa::all();
        return view('transaksi.create', compact('kontrakans', 'penyewas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tgl_sewa' => 'required',
            'harga' => 'required',
            'jenis_sewa' => 'required',
            'penyewa_id' => 'required',
            'kontrakan_id' => 'required'
        ]);

        $transaksi = Transaksi::create([
            "tgl_sewa" => $request["tgl_sewa"],
            "harga" => $request["harga"],
            "jenis_sewa" => $request["jenis_sewa"],
            "penyewa_id" => $request["penyewa_id"],
            "kontrakan_id" => $request["kontrakan_id"],
        ]);
        return redirect('/transaksi')->with('success', 'Berhasil Menambahkan Tranksaksi Baru!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaksi = Transaksi::find($id);
        $date = date_create($transaksi->tgl_sewa);
        $transaksi->tgl_sewa = date_format($date, "d/m/Y");
        return view('transaksi.show', compact('transaksi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaksi = Transaksi::find($id);
        $kontrakans = Kontrakan::all();
        $penyewas = Penyewa::all();
        return view('transaksi.edit', compact('transaksi', 'kontrakans', 'penyewas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tgl_sewa' => 'required',
            'harga' => 'required',
            'jenis_sewa' => 'required',
            'penyewa_id' => 'required',
            'kontrakan_id' => 'required'
        ]);

        $update = Transaksi::where('id', $id)->update([
            "tgl_sewa" => $request["tgl_sewa"],
            "harga" => $request["harga"],
            "jenis_sewa" => $request["jenis_sewa"],
            "penyewa_id" => $request["penyewa_id"],
            "kontrakan_id" => $request["kontrakan_id"],
        ]);
        return redirect('/transaksi')->with('success', 'Berhasil Merubah Data Transaksi!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Transaksi::destroy($id);
        return redirect('/transaksi')->with('success', 'Data Transaksi Berhasil Dihapus');
    }
}
