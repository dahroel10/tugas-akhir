@extends('layouts.master')
@section('content')
<div class="card mx-1">
    <img src="{{ asset('img/kontrakan/'. $kontrakan->foto) }}"width="500px" alt="...">
    <div class="card-body">
      <h5 class="card-title">{{ $kontrakan->name }}</h5>
      <p class="card-text">Pemilik : {{ $kontrakan->pemilik->name }}</h5>
      <p class="card-text">Alamat : {{ $kontrakan->alamat }}</p>
      <p class="card-text">Fasilitas : {{ $kontrakan->fasilitas }}</p>
      <p class="card-text">Jumlah Kontrakan Yang Tersedia : {{ $kontrakan->jumlah_kontrakan }}</p>
      <p class="card-text"><small class="text-muted">{{ $kontrakan->created_at }}</small></p>
      <a href="/kontrakan" class="btn btn-primary">Kembali</a>
    </div>
  </div>
@endsection