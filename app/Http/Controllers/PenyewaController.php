<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Kontrakan;
use App\Penyewa;
use Auth;
use File;

class PenyewaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penyewas = Penyewa::all();
        return view('penyewa.index', compact('penyewas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('penyewa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'jk'=>'required',
            'alamat'=>'required',
            'email'=>'required',
            'no_hp'=>'required',
            'no_ktp'=>'required',
            'pekerjaan'=>'required',
            'foto'=>'required|mimes:jpeg,jpg,png|max:2200'
        ]);

        $gambar = $request->foto;
        $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();

        $penyewa = Penyewa::create([
            "name"=>$request->name,
            "jk"=>$request["jk"],
            "alamat"=>$request["alamat"],
            "email"=>$request["email"],
            'no_hp'=>$request->no_hp,
            'no_ktp'=>$request->no_ktp,
            "pekerjaan"=>$request["pekerjaan"],
            "foto"=>$new_gambar
        ]);
        $gambar -> move('img/penyewa/', $new_gambar);
        return redirect('/penyewa')->with('success', 'Berhasil Menambahkan Penyewa Baru!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penyewa = Penyewa::find($id);
        return view('penyewa.show', compact('penyewa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penyewa = Penyewa::find($id);
        return view('penyewa.edit', compact('penyewa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'jk'=>'required',
            'alamat'=>'required',
            'email'=>'required',
            'no_hp'=>'required',
            'no_ktp'=>'required',
            'pekerjaan'=>'required',
            'foto'=>'mimes:jpeg,jpg,png|max:2200'
        ]);

        $penyewa = Penyewa::findorfail($id);
        
        if($request->has('foto')){
            $path = 'img/penyewa/';
            File::delete($path . $penyewa->foto);
            $gambar = $request->foto;
            $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();
            $gambar->move($path, $new_gambar);
            $penyewa_data = [
                'name'=>$request->name,
                'jk'=>$request->jk,
                'alamat'=>$request->alamat,
                'no_hp'=>$request->no_hp,
                'no_ktp'=>$request->no_ktp,
                'pekerjaan'=>$request->pekerjaan,
                'foto'=>$new_gambar
            ];
        } else{
            $penyewa_data = [
                'name'=>$request->name,
                'jk'=>$request->jk,
                'alamat'=>$request->alamat,
                'no_hp'=>$request->no_hp,
                'no_ktp'=>$request->no_ktp,
                'pekerjaan'=>$request->pekerjaan
            ];
        }
        $penyewa->update($penyewa_data);
        return redirect('/penyewa')->with('success', 'Berhasil Merubah Data penyewa!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $penyewa = Penyewa::findorfail($id);
        $penyewa -> delete();

        $path = "img/penyewa/";
        File::delete($path . $penyewa->foto);
        return redirect('/penyewa')->with('success', 'Data penyewa  Berhasil Dihapus');
    }
}
