<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SewaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sewa', function (Blueprint $table) {
        $table->unsignedBigInteger('pemilik_id');
        $table->foreign('pemilik_id')->references('id')->on('pemilik');  
        $table->unsignedBigInteger('transaksi_id');
        $table->foreign('transaksi_id')->references('id')->on('transaksi');
        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sewa');
        $table->dropForeign(['pemilik_id']);
        $table->dropColumn(['pemilik_id']);
        $table->dropForeign(['transaksi_id']);
        $table->dropColumn(['transaksi_id']);

    }
}
