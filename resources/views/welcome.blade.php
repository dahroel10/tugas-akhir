<!DOCTYPE html>
<html lang="en">

<head>
    <title>Stated &mdash; Website by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900|Playfair+Display:400,700,900 "
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('homepage/fonts/icomoon/style.css') }}">

    <link rel="stylesheet" href="{{ asset('homepage/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('homepage/css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('homepage/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('homepage/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('homepage/css/owl.theme.default.min.css') }}">

    <link rel="stylesheet" href="{{ asset('homepage/css/jquery.fancybox.min.css') }}">

    <link rel="stylesheet" href="{{ asset('homepage/css/bootstrap-datepicker.css') }}">

    <link rel="stylesheet" href="{{ asset('homepage/fonts/flaticon/font/flaticon.css') }}">

    <link rel="stylesheet" href="{{ asset('homepage/css/aos.css') }}">

    <link rel="stylesheet" href="{{ asset('homepage/css/style.css') }}">

</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

    <div class="site-wrap">

        <div class="site-mobile-menu site-navbar-target">
            <div class="site-mobile-menu-header">
                <div class="site-mobile-menu-close mt-3">
                    <span class="icon-close2 js-menu-toggle"></span>
                </div>
            </div>
            <div class="site-mobile-menu-body"></div>
        </div>


        <header class="site-navbar py-4 js-sticky-header site-navbar-target" role="banner">

            <div class="container">
                <div class="row align-items-center">

                    <div class="col-6 col-xl-2">
                        <h1 class="mb-0 site-logo m-0 p-0"><a href="index.html" class="mb-0">Stated.</a></h1>
                    </div>

                    <div class="col-12 col-md-10 d-none d-xl-block">
                        <nav class="site-navigation position-relative text-right" role="navigation">

                            <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                                @if (Route::has('login'))
                                    @auth
                                        <li><a href="{{ url('/home') }}" class="nav-link">Home</a></li>
                                    @else
                                        <li><a href="{{ route('login') }}" class="nav-link">Login</a></li>

                                        @if (Route::has('register'))
                                            <li><a href="{{ route('register') }}" class="nav-link">Register</a>
                                            </li>
                                        @endif
                                    @endauth
                                @endif
                            </ul>
                        </nav>
                    </div>


                    <div class="col-6 d-inline-block d-xl-none ml-md-0 py-3"><a href="#"
                            class="site-menu-toggle js-menu-toggle text-black float-right"><span
                                class="icon-menu h3"></span></a></div>

                </div>
            </div>

        </header>



        <div class="site-blocks-cover overlay"
            style="background-image: url({{ asset('homepage/images/hero_1.jpg') }});" data-aos="fade"
            id="home-section">


            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-6 mt-lg-5 text-center">
                        <h1>Sewa rumah atau kontrakan</h1>
                        <p class="mb-5">Cari rumah idaman dengan harga murah dan bersahabat</p>

                    </div>
                </div>
            </div>

            <a href="#howitworks-section" class="smoothscroll arrow-down"><span class="icon-arrow_downward"></span></a>
        </div>


        <div class="py-5 bg-light site-section how-it-works" id="howitworks-section">
            <div class="container">
                <div class="row mb-5 justify-content-center">
                    <div class="col-md-7 text-center">
                        <h2 class="section-title mb-3">How It Works</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <div class="pr-5">
                            <span class="custom-icon flaticon-house text-primary"></span>
                            <h3 class="text-dark">Find Home.</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                        </div>
                    </div>

                    <div class="col-md-4 text-center">
                        <div class="pr-5">
                            <span class="custom-icon flaticon-coin text-primary"></span>
                            <h3 class="text-dark">Rent Home.</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                        </div>
                    </div>

                    <div class="col-md-4 text-center">
                        <div class="pr-5">
                            <span class="custom-icon flaticon-home text-primary"></span>
                            <h3 class="text-dark">Live Happily.</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="site-section" id="properties-section">
            <div class="container">
                <div class="row mb-5 align-items-center">
                    <div class="col-md-7 text-left">
                        <h2 class="section-title mb-3">Rumah</h2>
                    </div>
                    <div class="col-md-5 text-left text-md-right">
                        <div class="custom-nav1">
                            <a href="#" class="custom-prev1">Previous</a><span class="mx-3">/</span><a
                                href="#" class="custom-next1">Next</a>
                        </div>
                    </div>
                </div>

                <div class="owl-carousel nonloop-block-13 mb-5">

                    <div class="property">
                        <a href="property-single.html">
                            <img src="{{ asset('homepage/images/property_1.jpg') }}" alt="Image"
                                class="img-fluid">
                        </a>
                        <div class="prop-details p-3">
                            <div><strong class="price">Rp.3,400,000</strong></div>
                            <div class="mb-2 d-flex justify-content-between">
                                <span class="w border-r">6 beds</span>
                                <span class="w border-r">4 baths</span>
                                <span class="w">4,250 sqft.</span>
                            </div>
                            <div>480 12th, Unit 14, San Francisco, CA</div>
                        </div>
                    </div>

                    <div class="property">
                        <a href="property-single.html">
                            <img src="{{ asset('homepage/images/property_2.jpg') }}" alt="Image"
                                class="img-fluid">
                        </a>
                        <div class="prop-details p-3">
                            <div><strong class="price">Rp.3,400,000</strong></div>
                            <div class="mb-2 d-flex justify-content-between">
                                <span class="w border-r">6 beds</span>
                                <span class="w border-r">4 baths</span>
                                <span class="w">4,250 sqft.</span>
                            </div>
                            <div>480 12th, Unit 14, San Francisco, CA</div>
                        </div>
                    </div>

                    <div class="property">
                        <a href="property-single.html">
                            <img src="{{ asset('homepage/images/property_3.jpg') }}" alt="Image"
                                class="img-fluid">
                        </a>
                        <div class="prop-details p-3">
                            <div><strong class="price">Rp.3,400,000</strong></div>
                            <div class="mb-2 d-flex justify-content-between">
                                <span class="w border-r">6 beds</span>
                                <span class="w border-r">4 baths</span>
                                <span class="w">4,250 sqft.</span>
                            </div>
                            <div>480 12th, Unit 14, San Francisco, CA</div>
                        </div>
                    </div>

                    <div class="property">
                        <a href="property-single.html">
                            <img src="{{ asset('homepage/images/property_4.jpg') }}" alt="Image"
                                class="img-fluid">
                        </a>
                        <div class="prop-details p-3">
                            <div><strong class="price">Rp.3,400,000</strong></div>
                            <div class="mb-2 d-flex justify-content-between">
                                <span class="w border-r">6 beds</span>
                                <span class="w border-r">4 baths</span>
                                <span class="w">4,250 sqft.</span>
                            </div>
                            <div>480 12th, Unit 14, San Francisco, CA</div>
                        </div>
                    </div>

                </div>
                <div class="row justify-content-center">
                    <div class="col-md-4">
                        <a href="#" class="btn btn-primary btn-block">View All Property Listings</a>
                    </div>
                </div>
            </div>
        </div>

        <footer class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-5">
                                <h2 class="footer-heading mb-4">About Stated</h2>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque facere laudantium
                                    magnam voluptatum autem. Amet aliquid nesciunt veritatis aliquam.</p>
                            </div>
                            <div class="col-md-3 ml-auto">
                                <h2 class="footer-heading mb-4">Quick Links</h2>
                                <ul class="list-unstyled">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">Testimonials</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-4">
                            <h2 class="footer-heading mb-4">Subscribe Newsletter</h2>
                            <form action="#" method="post" class="footer-subscribe">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control border-secondary text-white bg-transparent"
                                        placeholder="Enter Email" aria-label="Enter Email"
                                        aria-describedby="button-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary text-black" type="button"
                                            id="button-addon2">Send</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="">
              <h2 class=" footer-heading mb-4">Follow Us</h2>
                            <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                            <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                            <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                            <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
                        </div>


                    </div>
                </div>
                <div class="row pt-5 mt-5 text-center">
                    <div class="col-md-12">
                        <div class="border-top pt-5">
                            <p>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                <script>
                                    document.write(new Date().getFullYear());
                                </script> All rights reserved | This template is made with <i
                                    class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com"
                                    target="_blank">Colorlib</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </footer>

    </div> <!-- .site-wrap -->

    <script src="{{ asset('homepage/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('homepage/js/jquery-migrate-3.0.1.min.js') }}"></script>
    <script src="{{ asset('homepage/js/jquery-ui.js') }}""></script>
    <script src="{{ asset('homepage/js/popper.min.js') }}"></script>
    <script src="{{ asset('homepage/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('homepage/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('homepage/js/jquery.stellar.min.js') }}"></script>
    <script src="{{ asset('homepage/js/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('homepage/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('homepage/js/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('homepage/js/aos.js') }}"></script>
    <script src="{{ asset('homepage/js/jquery.fancybox.min.js') }}"></script>
    <script src="{{ asset('homepage/js/jquery.sticky.js') }}"></script>


    <script src="{{ asset('homepage/js/main.js') }}"></script>

</body>

</html>
